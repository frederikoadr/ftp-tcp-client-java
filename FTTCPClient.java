/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ft.tcp.client;

/**
 *
 * @author user
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.*;
import java.io.*;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class FTTCPClient {
        
        public static void main(String[] args)
        {
            try {
                System.out.println("Please enter a full file path");
                String fileName = new Scanner(System.in).nextLine();
                File f = new File(fileName);
                Path wiki_path = Paths.get(fileName);
                Socket socket = new Socket(InetAddress.getByName("localhost"), 1234);
                System.out.println("Connected. Sending file.");
                OutputStream out = socket.getOutputStream();
                PrintWriter sWriter = new PrintWriter(out);
                FileOutputStream fos = new FileOutputStream(fileName);
                byte[] bytes = Files.readAllBytes(wiki_path);
                String s = new String(bytes);
                sWriter.print(s);
                
                sWriter.print(fileName);
                sWriter.flush();
                System.out.println("Sending file");
                fos.write(bytes);
            }
            catch (Exception e) {
                System.out.println(e);
            }
            
            System.console().readLine();
        }
}
